<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Armstrong Identifier</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<style>
    .text1{
        font-family: fantasy;
    }
    .style{
        background-color:grey;
    }
    .design{
        margin-left:33%;
    }
</style>
<body class = "style">
    <br><br>
    <center><h3 class = "text1">Armstrong Number Identifier</h3>
    <br>
    <form class="row g-1 design" method = "post">
        <div class="col-auto">
            <input type="text" readonly class="form-control-plaintext" value="Input a number :">
        </div>
        <div class="col-auto">
            <input type="number" class="form-control"  placeholder="Enter a number.." name = "number">
        </div>
        <div class="col-auto">
            <button type="submit" name ="submit" class="btn btn-primary mb-3">Submit</button>
        </div>
    </form>
    <?php     
        if(isset($_POST['submit'])){
            $num = $_POST["number"];
            if($num==null){
                echo "<br><h4 style = font-familyfantasy> Input a number first!";
            }
            $total=0;
            $newnum=$num;    
            while($newnum!=0){  
                $remain=$newnum%10;  
                $total=$total+$remain*$remain*$remain;  
                $newnum=$newnum/10;  
            } 
            if($num==$total && $num!=null){
                echo "<br><h4 style = font-family:fantasy>" . $num ." is an Armstrong Number";  
            }  
            if($num!=$total){ 
                echo "<br><h4 style = font-family:fantasy>" . $num ." is not an Armstrong Number";  
            }  
        }    
    ?>
</body>
</html>

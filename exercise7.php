<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Convert Numbers to Words</title>
</head>
<style>
    .textd{
        font-size:20px;
    }
    .design{
        margin-left: 33%;
    }
</style>
<body style = "background-color:grey">
    <div class="container mt-5 ">
        <div class="row">
            <div class="card bg-info text-center ">
                <div class="card-header bg-dark text-white mt-2"><h5>Convert number into words!</h5></div>
                <div class="card-body">
                <form class="row g-1 design" method = "post" >
                        <div class="col-auto color">
                            <label for="inputPassword6" class="col-form-label">Input a number: </label>
                        </div>
                        <div class="col-auto">
                            <input type="number" class="form-control"  placeholder="Enter a number.." name = "num">
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary mb-3" name = "convert">Convert</button><br>  
                        </div>
                </form>        
                        <div>                 
                        <?php
                            function inputGetter($digit){              
                                switch($digit){
                                    
                                    case '0':
                                        echo "Zero ";
                                        break;
                                    case '1':
                                        echo "One ";
                                        break;
                                    case '2':
                                        echo "Two ";
                                        break;
                                    case '3':
                                        echo "Three ";
                                        break;
                                    case '4':
                                        echo "Four ";
                                        break;
                                    case '5':
                                        echo "Five ";
                                        break;
                                    case '6':
                                        echo "Six ";
                                        break;
                                    case '7':
                                        echo "Seven ";
                                        break;
                                    case '8':
                                        echo "Eight ";
                                        break;
                                    case '9':
                                        echo "Nine ";
                                        break;
                                                        
                                }

                            }
                            function converted($inputNum){
                                $length = strlen((int)$inputNum);
                                for ($ittirator=0; $ittirator <$length ; $ittirator++) { 
                                    inputGetter($inputNum[$ittirator]);
                                }

                            }
                            if(isset($_POST['convert'])){
                                $inputNum = $_POST['num'];
                                converted($inputNum);
                            }
                        ?>
                        </div>                   
                </div>
            </div>
        </div>
    </div>
</body>
</html>
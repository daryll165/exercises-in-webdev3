<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Division Table</title>
</head>
<style>
     .table{
    background-color:lightgrey;
    }
    th,td{
    border:2px solid lightblue;
    color:white;
    text-align: center;
    font-family:monospace;
    }
    .tstyle{
        font-family:fantasy;
    }
</style>
<body style = "background-color:grey">
    <div class="container mt-5">
        <div class="row">
            <div class="card bg-dark">
            <table border = 1>
                <div class="card-header bg-info text-black text-center mt-2 tstyle"><h5>Division Table</h5></div>
                <div class="card-body">
                <?php
                $start = 1;
                $end = 15;
                echo("<th style=border:2px solid white> </th>");
                for ($count = $start;$count <= $end;$count++)
                    echo("<th style=font-size:26px>$count</th>");    
                for ($count = $start;$count <= $end;$count++){
                    echo("<tr><th style=font-size:24px>$count</th>");
                    for ($counter = $start;$counter <= $end;$counter++){
                        $result = $count / $counter;
                        printf("<td>%.1f</td>",$result);
                    }
                    echo("</tr>\n");
                }
                ?> 
                </div>
            </div>
        </div>
    </div>
</body>
</html>

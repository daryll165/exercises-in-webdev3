<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Products</title>
</head>
<style>
    .textd{
        font-size:20px;
    }
</style>
<body style = "background-color:grey">
    <div class="container mt-5 ">
        <div class="row">
            <div class="card bg-info text-center textd">
                <div class="card-header bg-dark text-white text-center mt-2"><h5>Find the best deal to purchase</h5></div>
                <div class="card-body">
                <?php
                    $quantity1 = 70;
                    $quantity2 = 100;
                    $price1 = 35;
                    $price2 = 30;
                    $total1 = $quantity1 * $price1;
                    $total2 = $quantity2 * $price2;
                    echo "<li style = font-family:monospace>Product 1 with a Quantity of : ". $quantity1 ."pcs<br>";  
                    echo "<li style = font-family:monospace>Product 2 with a Quantity of : " . $quantity2 ."pcs<br>";
                    echo "<li style = font-family:monospace>Price for Product 1 : ₱" . $price1 ."<br>";
                    echo "<li style = font-family:monospace>Price for Product 2 : ₱" . $price2 ."<br>";
                    if ($total1<$total2) {
                        echo "<br>" . "The best deal to purchase is Product 1 with a Quantity of " . $quantity1 . "pcs and having a Price of ₱" .$price1 . ".";
                    } else {
                        echo "<br>" . "The best deal to purchase is Product 2 with a Quantity of " . $quantity2 . "pcs and having a Price of ₱" .$price2 . ".";
                    }                            
                ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Student Data</title>
</head>
<body style = "background-color:grey">
    <div class="container mt-5 ">
        <div class="row">
            <div class="card bg-info">
                <div class="card-header bg-dark text-white text-center mt-2"><h5>List of Students</h5></div>
                <div class="card-body">
                <?php
                    $studentData= '[
                        {
                        "name" : "John Garg",
                        "age"  : "15",
                        "school" : "Ahlcon Public School"
                        },
                        {
                        "name" : "Smith Soy",
                        "age"  : "16",
                        "school" : "St. Marie School"
                        },
                        {
                        "name" : "Charle Rena",
                        "age"  : "16",
                        "school" : "St. Columba School"
                        }
                    ]';
                    $break = json_decode($studentData, true);
                    foreach ($break as $key => $values) {
                        echo "<h3 style=font-family:monospace>Name : ". $values["name"] ."<br>"."<hr>"."Age : ".$values["age"] . "<br>" ."<hr>"."School : " .$values["school"] . "<br>"."<hr>";
                        echo "<br>";
                    }
                ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Recurring Strings</title>
</head>
<body style = "background-color:grey">
    <div class="container mt-5">
        <div class="row">
            <div class="card">
                <div class="card-header bg-info text-black text-center mt-2"><h5> A program to deletes the recurring elements inside a sorted list of strings.</h5></div>
                <div class="card-body">
                    <?php
                        $fruits = array("lemon", "orange", "banana", "apple","lemon","watermelon","pandan","guava",);
                        sort($fruits);
                        $length = count($fruits);
                        echo "<h5>Sorted list of Fruits</h5>";
                        for($i=0;$i<$length;$i++){
                            echo $fruits[$i] . "<br>";
                        }
                        echo "<h3>Output</h3>";
                        $new = array_unique($fruits);
                        print_r($new);
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>